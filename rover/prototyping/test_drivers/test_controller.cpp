#include <controller.h>
#include <unity.h>

void test_controller_algorithm_throttle(void){
    int lt, rt;    

    rc_controller_controll(100, 0, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(100, rt);

    rc_controller_controll(0, 0, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(0, lt);
    TEST_ASSERT_EQUAL_INT32(0, rt);

    rc_controller_controll(50, 0, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(50, lt);
    TEST_ASSERT_EQUAL_INT32(50, rt);

    rc_controller_controll(200, 0, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(0, lt);
    TEST_ASSERT_EQUAL_INT32(0, rt);

    rc_controller_controll(-100, 0, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(0, lt);
    TEST_ASSERT_EQUAL_INT32(0, rt);
}


void test_controller_algorithm_yaw(void){
    int lt, rt;    

    rc_controller_controll(100, 100, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(-100, rt);

    rc_controller_controll(0, 100, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(-100, rt);

    rc_controller_controll(50, 100, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(-100, rt);

    rc_controller_controll(10, 100, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(-100, rt);

    rc_controller_controll(100, 50, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(0, rt);

    rc_controller_controll(100, 25, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(50, rt);

    rc_controller_controll(100, 75, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(100, lt);
    TEST_ASSERT_EQUAL_INT32(-50, rt);

    rc_controller_controll(50, 50, &lt, &rt);
    TEST_ASSERT_EQUAL_INT32(75, lt);
    TEST_ASSERT_EQUAL_INT32(-25, rt);

    // rc_controller_controll(50, 75, &lt, &rt);
    // TEST_ASSERT_EQUAL_INT32(100, lt);
    // TEST_ASSERT_EQUAL_INT32(100, rt);

    // rc_controller_controll(-100, 0, &lt, &rt);
    // TEST_ASSERT_EQUAL_INT32(100, lt);
    // TEST_ASSERT_EQUAL_INT32(100, rt);
}


