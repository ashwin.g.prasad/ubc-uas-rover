#include <controller/autodriver_controller.h>
#include <controller/pid.h>
#include <GPScontroller.h>
#include <stdlib.h>

bool AutoDrive(RoverGPSDriver gps, RoverL298NMotorDriver left_motor, RoverL298NMotorDriver right_motor, double currentHeading)
{
  
    MotorMiddleware motor = MotorMiddleware(left_motor, right_motor);
    double currentlat = gps.current_position.lat;
    double currentlon = gps.current_position.lon;
    //double prevlat = gps.last_position.lat;
    //double prevlon = gps.last_position.lon;

    double * targetAngle;
    double * distanceToTarget;

    GpsControllerGetData(currentlat, currentlon, targetAngle, distanceToTarget);

    motor.MotorController(HeadingPIDController(currentHeading, *targetAngle));

    if (*distanceToTarget < TARGET_MIN_DISTANCE) {
        return true;
    }
    return false;
}

