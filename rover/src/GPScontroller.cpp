#include <GPScontroller.h>
#include <global_constants.h>
#include <math.h>

//Forward function declarations
double CalculateDistance(double nLat1, double nLon1, double nLat2, double nLon2);
double angleFromCoordinate(double lat1, double long1, double lat2, double long2);
double ToRad(double angle);
double toDegrees(double angle);

double targetLatitude = 79.42;
double targetLongitude = 739262.272; 


bool GpsControllerGetData(double latitude, double longitude, double* targetAngle, double* distanceToTarget)
{
    if (latitude == 0 || longitude == 0)
    {
        return false;
    }

    *targetAngle = angleFromCoordinate(latitude, longitude, targetLatitude, targetLongitude);
    *distanceToTarget = CalculateDistance(latitude, longitude, targetLatitude, targetLongitude);

    return true;
}


double CalculateDistance(double nLat1, double nLon1, double nLat2, double nLon2)
{
     
    // Get the difference between our two points
    // then convert the difference into radians
 
    double nDLat = ToRad(nLat2 - nLat1);
    double nDLon = ToRad(nLon2 - nLon1);
 
    // Here is the new line
    nLat1 =  ToRad(nLat1);
    nLat2 =  ToRad(nLat2);
 
    double nA = pow ( sin(nDLat/2), 2 ) + cos(nLat1) * cos(nLat2) * pow ( sin(nDLon/2), 2 );
 
    double nC = 2 * atan2( sqrt(nA), sqrt( 1 - nA ));
    double nD = EARTH_RADIUS_KM * nC;
 
    return nD; // Return our calculated distance
}

double angleFromCoordinate(double lat1, double long1, double lat2, double long2) 
{
    double dLon = (long2 - long1);

    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1)
            * cos(lat2) * cos(dLon);

    double brng = atan2(y, x);

    brng = toDegrees(brng);
    brng = fmod((brng + 360),360);
    brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

    return brng;
}

//Converts angle to radians
double ToRad(double angle)
{
   double radians;
   radians = (angle*M_PI)/180;
   return radians;
}

double toDegrees(double angle)
{
   double degrees;
   degrees = (angle*M_PI)/180;
   return degrees;
}