#include <drivers/rover_l298n_motor_driver.h>
#include <Arduino.h>

RoverL298NMotorDriver::RoverL298NMotorDriver(){

}

RoverL298NMotorDriver::RoverL298NMotorDriver(pin_t motor_in1, pin_t motor_in2, pin_t enA){
    // set up attributes
    motor_in1_pin = motor_in1;
    motor_in2_pin = motor_in2;
    motor_enA_pin = enA;

    // initialize pins
    pinMode(motor_in1_pin, OUTPUT);
    pinMode(motor_in2_pin, OUTPUT);
    pinMode(motor_enA_pin, OUTPUT);

    motor_controll_pin = motor_enA_pin;
    
    // set default values.
    digitalWrite(motor_enA_pin, LOW);

    motor_direction = true;

    motor_percent = 0;
}

void RoverL298NMotorDriver::ChangeSpeed(int percent){
    motor_percent = percent;
}

void RoverL298NMotorDriver::UpdateSpeed() {
    if (motor_percent == 0){
        analogWrite(motor_controll_pin, 0);
        digitalWrite(motor_in1_pin, LOW);
        digitalWrite(motor_in2_pin, LOW);
    }else{
        // reverse direction is needed. 
        if ((motor_percent > 0 && !motor_direction) || (motor_percent < 0 && motor_direction)){
            ReverseDirection();
        }
        analogWrite(motor_controll_pin, map(abs(motor_percent), NO_POWER, FULL_POWER, PWM_LOW, PWM_HIGH));      
    }
}

void RoverL298NMotorDriver::ReverseDirection(){
    motor_direction = !motor_direction;

    if (motor_direction){
        digitalWrite(motor_in1_pin, HIGH);
        digitalWrite(motor_in2_pin, LOW); 
    }else{
        digitalWrite(motor_in1_pin, LOW);
        digitalWrite(motor_in2_pin, HIGH); 
    }
}
