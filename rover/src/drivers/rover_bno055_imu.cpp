#include <drivers/rover_bno055_imu.h>
#include <type.h>

RoverBNO055IMU::RoverBNO055IMU(){

}

RoverBNO055IMU::RoverBNO055IMU(pin_t inputSCL, pin_t inputSDA){
    SCL = inputSCL;
    SDA = inputSDA;
    // TODO need 
    bno = Adafruit_BNO055(55, 0x28);
    bno.getSensor(&sensor);
    if(!bno.begin())
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
        while(1);
    }
    bno.setExtCrystalUse(true);
}

RoverBNO055IMU::~RoverBNO055IMU(){
    // ~Adafruit_BNO055();
}

double RoverBNO055IMU::getHeading(){
    sensors_event_t event;

    bno.getEvent(&event);

    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

    return euler.x();
    
    delay(BNO055_SAMPLERATE_DELAY_MS);
}