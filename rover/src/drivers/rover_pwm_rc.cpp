#include <drivers/rover_pwm_rc.h>
#include <Arduino.h>

RoverRC::RoverRC(){

}

RoverRC::RoverRC(pin_t rc_pin){
    pinMode(rc_pin, INPUT);
    rc_channel_pin = rc_pin;
}


bool RoverRC::ReadRCValue(){
    // TODO Use http://www.camelsoftware.com/2015/12/25/reading-pwm-signals-from-an-rc-receiver-with-arduino/ 
    pwm_value = pulseIn(this->rc_channel_pin, HIGH);

    if (!pwm_value){
        return false;
    }
    
    return true;
}