#include <drivers/rover_6m_gps.h>
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

RoverGPSDriver::RoverGPSDriver(){

}

RoverGPSDriver::RoverGPSDriver(pin_t tx, pin_t rx, int GPS_DELAY){
  _gps_delay = GPS_DELAY;
  _serialGPS.begin(9600);
}

// FIXME teensy smart delay!
// void RoverGPSDriver::SmartDelay(unsigned long ms)
// {
//   unsigned long start = millis();
//   do
//   {
//     while (Serial1.available())
//       gps.encode(Serial1.read());
//   } while (millis() - start < ms);
// }

void RoverGPSDriver::SmartDelay(unsigned long ms)
{
  unsigned long start = millis();
  // #ifdef
  do
  {
    while (_serialGPS.available())
    digitalWrite(13, HIGH);
      _tinyGPS.encode(_serialGPS.read());
      _serialGPS.read();
      digitalWrite(13, LOW);
      digitalWrite(13, HIGH);
      _tinyGPS.encode(Serial.read());
  } while (millis() - start < ms);

  //   do
  // {
  //   while (Serial1.available())
  //     gps.encode(Serial1.read());
  // } while (millis() - start < ms);
}


void RoverGPSDriver::UpdatePositions(){
  current_position.lat = _tinyGPS.location.lat();
  current_position.lon = _tinyGPS.location.lng();
  SmartDelay(_gps_delay);

  // Serial.print("Last position 1 ", last_position.lat, last_position.lon);
  last_position.lat = current_position.lat;
  last_position.lon = current_position.lon;
  // Serial.print("Last position 2 %lf, %lf\n", last_position.lat, last_position.lon);
}
