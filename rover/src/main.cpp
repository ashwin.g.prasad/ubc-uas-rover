#if defined(ARDUINO) && ARDUINO >= 100
  #include <Arduino.h>
#else
	#include "WProgram.h"
#endif

#include <Ticker.h>

#include <drivers/rover_l298n_motor_driver.h>
#include <drivers/rover_6m_gps.h>
#include <drivers/rover_pwm_rc.h>
#include <controller/autodriver_controller.h>
#include <TinyGPS++.h>

/*
 * 0: Attached to the rope
 * 1: Start deattahing the rope
 * 2: Deattaching the rope
 * 3: Rope deattached
 * 4: Wait
 * 5: Drive
 * 6: Stop
 */

#define GPS_TX 9
#define GPS_RX 8
#define GPS_DELAY 1000

#define RC_THROTTLE_PIN 3
#define RC_YAW_PIN 4

#define MOTOR_PIN_L_ENA 5
#define MOTOR_PIN_L_IN1 6
#define MOTOR_PIN_L_IN2 7

#define MOTOR_PIN_R_ENA 10
#define MOTOR_PIN_R_IN1 11
#define MOTOR_PIN_R_IN2 12


#define RC_PWM_HIGH 2000
#define RC_PWM_LOW 1000

#define THROTTLE_RANGE_LOW 0
#define THROTTLE_RANGE_HIGH 100

#define YAW_RANGE_LOW -100
#define YAW_RANGE_CENTER 0
#define YAW_RANGE_HIGH 100

RoverL298NMotorDriver left_motor;
RoverL298NMotorDriver right_motor;
RoverGPSDriver rover_gps;
RoverRC throttle_channel;
RoverRC yaw_channel;
double current_heading;
bool auto_complete;

// callbacks
void RCUpdateCallback() {
  throttle_channel.ReadRCValue();
  yaw_channel.ReadRCValue();
}

void MotorCallback() {
  left_motor.UpdateSpeed();
  right_motor.UpdateSpeed();
}

void rover_gpsDriverCallback(){
  rover_gps.UpdatePositions();
  current_heading = TinyGPSPlus::courseTo(
    rover_gps.current_position.lat,
    rover_gps.current_position.lon,
    rover_gps.last_position.lat,
    rover_gps.last_position.lon);
}


Ticker rcUpdate(RCUpdateCallback, 100, 0);
Ticker motorUpdate(MotorCallback, 100, 0);
Ticker gpsUpdate(rover_gpsDriverCallback, GPS_DELAY, 0);

void setup()
{
  Serial.begin(9600);
  // set up everything
  // motor middleware set up
  left_motor = RoverL298NMotorDriver(MOTOR_PIN_L_IN1, MOTOR_PIN_L_IN2, MOTOR_PIN_L_ENA);
  right_motor = RoverL298NMotorDriver(MOTOR_PIN_R_IN1, MOTOR_PIN_R_IN2, MOTOR_PIN_R_ENA);
  throttle_channel = RoverRC(RC_THROTTLE_PIN);
  yaw_channel = RoverRC(RC_YAW_PIN);
  rover_gps = RoverGPSDriver(GPS_TX, GPS_RX, GPS_DELAY);

  rcUpdate.start();
  motorUpdate.start();
  gpsUpdate.start();
}

void loop()
{

  // state checking
  // rc switch support

  // rover drivering pareameter list
  // gps_object, left motor, right motor, current_heading

  rcUpdate.update();
  motorUpdate.update();
  gpsUpdate.update();
  // heading

  if (!auto_complete) {
    auto_complete = AutoDrive(rover_gps, left_motor, right_motor, current_heading);
  } else {
    // motor function call termination function
  }


}