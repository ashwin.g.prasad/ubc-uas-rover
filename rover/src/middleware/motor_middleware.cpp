#include <middleware/motor_middleware.h>
#include <drivers/rover_l298n_motor_driver.h>
#include <Arduino.h>

//MotorMiddleware
MotorMiddleware::MotorMiddleware(RoverL298NMotorDriver left, RoverL298NMotorDriver right){
    left_motor = left;
    right_motor = right;
}
void MotorMiddleware::MotorController(int turn_angle){
    int time_turn = turn_angle/360 * TIME_FOR_ONE_ROTATION + TIME_SPEED_UP;
    int time_left = REFRESH_RATE - time_turn - TRANSITION_DELAY;

    if(turn_angle > 0){
        left_motor.ChangeSpeed(FULL_POWER);
        right_motor.ChangeSpeed(-FULL_POWER);
    }
    else{
        left_motor.ChangeSpeed(-FULL_POWER);
        right_motor.ChangeSpeed(FULL_POWER);
    }
    delay(time_turn);

    left_motor.ChangeSpeed(NO_POWER);
    right_motor.ChangeSpeed(NO_POWER);
    delay(TRANSITION_DELAY);

    left_motor.ChangeSpeed(FULL_POWER);
    right_motor.ChangeSpeed(FULL_POWER);
    delay(time_left);
}

