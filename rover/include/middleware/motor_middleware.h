#ifndef _MOTOR_H_
#define _MOTOR_H_
#include <drivers/rover_l298n_motor_driver.h>

#define TIME_FOR_ONE_ROTATION 2000
#define TIME_SPEED_UP 100
#define REFRESH_RATE 5000
#define TRANSITION_DELAY 200

class MotorMiddleware {
    private:
        int left_motor_percent;
        int right_motor_percent;
        RoverL298NMotorDriver left_motor;
        RoverL298NMotorDriver right_motor; 
    public:
        MotorMiddleware(RoverL298NMotorDriver left_motor, RoverL298NMotorDriver right_motor); 
        void MotorController(int turn_angle);

};
#endif

