#ifndef _LANDING_H_
#define _LANDING_H_
#include <drivers/rover_sharp2y0a21_ir_sensor.h>
class LandingMiddleware{
public:
IR ir;
XBEE xbee;
int distance;
bool LandingDetection();
void SlowDown();
}
#endif