#ifndef ROVER_RC_H_
#define ROVER_RC_H_

#include <type.h>

class RoverRC{
    public:    
        // the pwm value of a rc channel is directly accessable.
        int pwm_value;

        RoverRC();

        RoverRC(pin_t rc_pin);

        /**
         * Read the radio receiver value and save it.
         */ 
        bool ReadRCValue();
    
    private: 
        pin_t rc_channel_pin;
};

#endif 
