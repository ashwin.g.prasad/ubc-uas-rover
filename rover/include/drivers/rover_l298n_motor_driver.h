#ifndef _ROVER_L298N_MOTOR_DRIVER_H_
#define _ROVER_L298N_MOTOR_DRIVER_H_

#include <type.h>

/**
 * See L298N motor driver's datasheet at: https://www.sparkfun.com/datasheets/Robotics/L298_H_Bridge.pdf
 */

#define NO_POWER 0 
#define FULL_POWER 100

#define PWM_LOW 0
#define PWM_HIGH 255

class RoverL298NMotorDriver{
    private:
        pin_t motor_in1_pin;
        pin_t motor_in2_pin;  
        pin_t motor_controll_pin;
        pin_t motor_enA_pin;

        bool motor_direction;
        int motor_percent;

        /**
         * Reserve the motor direction.
         */ 
        void ReverseDirection();   
        
    public:
        RoverL298NMotorDriver();

        /**
         * Initialize the motor control pins.
         */ 
        RoverL298NMotorDriver(pin_t motor_in1, pin_t motor_in2, pin_t enA);

        /**
         * Change the speed of motor based on the percentage it's set to
         * Parameter:
         *      int speed: range from +100 to -100, where the sign indicate the direction
         *      of the motor (positive is forward, negative is backward), and percentage 
         *      signals the power output of the motor. 100 (means 100%) is full power and 0
         *      is no power output. 
         */
        void ChangeSpeed(int percent); 

        void UpdateSpeed();  
};
#endif
