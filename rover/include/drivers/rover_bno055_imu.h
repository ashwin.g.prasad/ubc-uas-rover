#ifndef _ROVER_BNO055_IMU_H_
#define _ROVER_BNO055_IMU_H_

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <type.h>

#define BNO055_SAMPLERATE_DELAY_MS (100)
class RoverBNO055IMU
{
private:
    /* data */
    Adafruit_BNO055 bno;
    pin_t SCL;
    pin_t SDA;
    sensor_t sensor;
public:
    RoverBNO055IMU();
    RoverBNO055IMU(pin_t SCL, pin_t SDA);
    ~RoverBNO055IMU();

    double getHeading();
};




#endif 