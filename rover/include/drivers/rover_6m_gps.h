#ifndef _ROVER_6M_GPS_H
#define _ROVER_6M_GPS_H

#include <TinyGPS++.h>
#include <type.h>

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "AltSoftSerial.h"

class RoverGPSDriver {
    private:
        pin_t _TXPin;
        pin_t _RXPin;
        AltSoftSerial _serialGPS;
        TinyGPSPlus _tinyGPS;
        unsigned long _gps_delay;
    public:
        struct gpsReading{
            double lat;
            double lon;
        }current_position, last_position;

        RoverGPSDriver();
        RoverGPSDriver(pin_t tx, pin_t rx, int GPS_DELAY);

        void UpdatePositions();

        void SmartDelay(unsigned long ms);
};


#endif
