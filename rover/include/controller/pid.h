#ifndef _PID_H_
#define _PID_H_

#include <Arduino.h>

#define INTEGRAL_ACTIVE_ZONE 1000 //need to change this value
#define K_P 0.0
#define K_D 0.0
#define K_I 0.0
//#define DELTA_T 0.0

struct PIDVariables{
    int error;
    float proportional;
    float derivative;
    float integral;
    float prev_error = 0;
    long long int last_time = millis();
};

//void PIDinit(PIDVariables* pid);
double HeadingPIDController(double current_heading, double target_heading);

#endif