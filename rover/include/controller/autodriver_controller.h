#ifndef _AUTODRIVER_CONTROLLER_H_
#define _AUTODRIVER_CONTROLLER_H_
#include <drivers/rover_6m_gps.h>
#include <middleware/motor_middleware.h>

bool AutoDrive( RoverGPSDriver gps, 
                RoverL298NMotorDriver left_motor, 
                RoverL298NMotorDriver right_motor, 
                double current_heading);

#endif //AUTODRIVER_CONTROLLER