#ifndef GPSCONTROLLER_H_
#define GPSCONTROLLER_H_

#include <drivers/rover_6m_gps.h>

#define TARGET_MIN_DISTANCE 5

void GpsControllerSetup();

void GpsControllerLoop();

bool GpsControllerGetData(double latitude, double longitude, double* targetAngle, double* distanceToTarget);


#endif // GPSCONTROLLER_H_
